import validator from 'validator';

console.log(validator.isEmail("coucou"));

const emails = ['mireillemathieu@gmail.com','patrickbruel@sfr.fr','jhallyday.com','claireguillerminet@hotmail.com']

function isValidEmail(email) {
    return validator.isEmail(email)
}

for (const email of emails) {
    console.log(email+"-"+ validator.isEmail(email));
}
